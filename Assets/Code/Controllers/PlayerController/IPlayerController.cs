﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPawnController
{
    void Move(Vector2 velocity);
    void Fire();
}