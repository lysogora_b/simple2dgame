﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : IPawnController
{
    public IPlayer Player { get; set; }

    public void Move(Vector2 velocity)
    {
        Player.Move(velocity);
    }

    public void Fire()
    {
        Player.Fire();
    }
}
