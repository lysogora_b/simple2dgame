﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Process input and pass it to the controller
/// </summary>
public interface IInputManager
{
	IPawnController Controller { get; set; }
}
