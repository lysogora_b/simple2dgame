﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeybordInputManager : MonoBehaviour, IInputManager
{
    public IPawnController Controller { get; set; }

    private void Update()
    {
        if (Controller == null) return;
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        Controller.Move(new Vector2(x, y));

        if (Input.GetKeyDown(KeyCode.Space)) Controller.Fire();

    }
}
