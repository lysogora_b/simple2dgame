﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public event Action StartGame ;
    [SerializeField]
    private GameObject _playPanel;

    public void StartPlay()
    {
        if (StartGame != null) StartGame();
    }

    public void ShowStartPanel()
    {
        _playPanel.SetActive(true);
    }
    
    public void HideStartPanel()
    {
        _playPanel.SetActive(false);
    }
}
