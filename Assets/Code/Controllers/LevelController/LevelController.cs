﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : ILevelController
{
	private ILevelView _levelView;

	public ILevelView LevelView
	{
		get { return _levelView; }
		set { _levelView = value; }
	}

	public void InitializeLevel()
	{
		_levelView.InitializeLevel();
	}

	public void StartGame()
	{
		_levelView.StartLevel();
	}

	public void EndGame()
	{
		
	}

	public void PlacePlayer(IPlayer player)
	{
		player.PlayerView.TransformView.position = _levelView.PlayerSpawnPoint.position;
		player.PlayerView.TransformView.SetParent(_levelView.PlayerSpawnPoint.parent);
	}
}