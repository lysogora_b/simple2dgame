﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides high level control of the game level.
/// </summary>
public interface ILevelController
{
	ILevelView LevelView { get; set; }
	/// <summary>
	/// prepare level for the start of the play.
	/// </summary>
	void InitializeLevel();

	void StartGame();
	
	void EndGame();
	/// <summary>
	/// Places player to the starting point of the level.
	/// </summary>
	/// <param name="player"></param>
	void PlacePlayer(IPlayer player);
}
