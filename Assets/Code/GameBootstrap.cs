﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Entry point of the game.
/// </summary>
public class GameBootstrap : MonoBehaviour
{
	private ObstaclesSpawner[] _obstacleSpawners;

	private Game _game;
	[SerializeField]
	private UIController _uiController;
	[SerializeField]
	private DataProvider _dataProvider;
	
	
	public void Awake ()
	{
		// create logic of the game
		_game = new Game(_dataProvider);

		// connect logic and view through events
		_uiController.StartGame += _game.StartGame;
		_game.OnGameCompleted += _uiController.ShowStartPanel;
		_game.OnGameStarted += _uiController.HideStartPanel;
	}

}
