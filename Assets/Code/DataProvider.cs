﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Provides necessary data and services for the logic layer. Does all respective necessary actions at the view layer.
/// </summary>
public class DataProvider : MonoBehaviour, IDataProvider
{
	[SerializeField]
	private GameObject playerPrefab;
	
	public event Action<LevelController> OnLevelReady;
	public event Action OnLevelCleared;

	private IInputManager _inputManager;

	private IPawnController _playerController;
	
	public IPlayer GetPlayer()
	{
		// instantiate player view
		var playerGo = Instantiate(playerPrefab) as GameObject;
		var playerView = playerGo.GetComponent<IPlayerView>();
		// create player and inject its view
		var player = new Player() {PlayerView = playerView};
		player.HitPoints = 1;
		player.PlayerView = playerView;
		playerView.Player = player;
		// subscribe to necessary events
		player.OnPlayerDeath += ProcessPlayerDeath;
		// set the way player will be controlled
		if (_inputManager == null)
		{
			_inputManager = gameObject.AddComponent<KeybordInputManager>();
		}

		_playerController = new PlayerController(){Player = player};
		_inputManager.Controller = _playerController;

		return player;
	}

	public void GetLevel(string levelName)
	{
		StartCoroutine(LeveLoadCoroutine(levelName));
	}

	public void ClearLevel(string levelName)
	{
		StartCoroutine(LeveClearCoroutine(levelName));
	}

	private IEnumerator LeveLoadCoroutine(string levelName)
	{
		// load level scene
		AsyncOperation ap = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
		while (!ap.isDone)
		{
			yield return null;
		}
		// get level view and put it into the level controller
		var levelViewGo = GameObject.Find("LevelView");
		var levelView = levelViewGo.GetComponent<LevelView>();
		levelView.InitializeLevel();
		var levelController = new LevelController(){LevelView =  levelView};
 
		if (OnLevelReady != null) OnLevelReady(levelController);
	}

	
	private IEnumerator LeveClearCoroutine(string levelName)
	{
		// load level scene
		AsyncOperation ap = SceneManager.UnloadSceneAsync(levelName);
		while (!ap.isDone)
		{
			yield return null;
		}
		
		if (OnLevelCleared != null) OnLevelCleared();
	}
	
	private void ProcessPlayerDeath()
	{
		_playerController = null;
		_inputManager.Controller = null;
	}
}
