﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game
{
    public event Action OnGameStarted;
    public event Action OnGameCompleted;

    private IDataProvider _dataProvider;
    private ILevelController _levelController;
    public IPlayer Player;


    public Game(IDataProvider dataProvider)
    {
        _dataProvider = dataProvider;
        _dataProvider.OnLevelCleared += OnLevelCleared;
        _dataProvider.OnLevelReady += OnLevelReady;
    }

    public void StartGame()
    {
        _dataProvider.GetLevel("Level");
    }

    private void OnLevelReady(LevelController levelController)
    {
        _levelController = levelController;
        Player = _dataProvider.GetPlayer();
        Player.OnPlayerDeath += EndGame;
        _levelController.PlacePlayer(Player);
        _levelController.StartGame();
        
        if (OnGameStarted != null) OnGameStarted();
    }

    public void EndGame()
    {
        _dataProvider.ClearLevel("Level");
    }

    private void OnLevelCleared()
    {
        _levelController = null;
        
        if (OnGameCompleted != null) OnGameCompleted();
    }

}
