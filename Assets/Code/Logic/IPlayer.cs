﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer 
{
    IPlayerView PlayerView { get; set; }
    event Action OnPlayerDeath;
    float HitPoints { get; set; }
    void Move(Vector2 velocity);
    void Fire();
}
