﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : IPlayer 
{
       private float _hitPoints;
       
       public float HitPoints
       {
              get { return _hitPoints; }
              set
              {
                     if (_hitPoints != value)
                     {
                            _hitPoints = value;
                            CheckLife();
                     }
              }
       }

       public IPlayerView PlayerView { get; set; }

       public event Action OnPlayerDeath;
       public event Action OnPlayerfire;
       
       public void Move(Vector2 velocity)
       {
              PlayerView.Move(velocity);
       }
       
       public void Fire()
       {
              PlayerView.Fire();
       }
       
       private void CheckLife()
       {
              if (_hitPoints <= 0)
              {
                     PlayerView.DestroyPlayer();
                     if (OnPlayerDeath != null) OnPlayerDeath();
              }
       }
}


