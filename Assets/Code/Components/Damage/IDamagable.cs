﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes object sensitive to the damage.
/// </summary>
public interface IDamagable
{
    void ReceiveDamage(int damage);
}