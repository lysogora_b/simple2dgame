﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagableComponent : MonoBehaviour, IDamagable 
{
    [SerializeField]
    private int _hitPoints;
       
    public int HitPoints
    {
        get { return _hitPoints; }
        set
        {
            if (_hitPoints != value)
            {
                _hitPoints = value;
                CheckLife();
            }
        }
    }
    
    public void ReceiveDamage(int damage)
    {
        HitPoints -= damage;
    }
    
    private void CheckLife()
    {
        if (_hitPoints <= 0)
        {
            Destroy(gameObject);
        }
    }
}
