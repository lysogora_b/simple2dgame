﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageComponent : MonoBehaviour
{
    [SerializeField]
    private int _damageAmount;
    [SerializeField]
    private string _targetTag;
    [SerializeField, Tooltip("This GameObject will be destroyed after causing damage")]
    private bool _destroyUponDamage;
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag(_targetTag)) return;
        var damagable = other.gameObject.GetComponent<IDamagable>();
        if (damagable == null) return;
        damagable.ReceiveDamage(_damageAmount);

        if (_destroyUponDamage)
        {
            Destroy(gameObject);
        }
    }
}
