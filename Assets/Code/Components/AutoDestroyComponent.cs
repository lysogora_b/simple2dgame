﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyComponent : MonoBehaviour
{
    [SerializeField]
    private float _lifeTime;
    
    void Awake()
    {
        Destroy(gameObject, _lifeTime);
    }
}
