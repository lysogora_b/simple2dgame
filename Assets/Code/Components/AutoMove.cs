﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour
{
    public Vector2 Velocity;
    public Rigidbody2D _rigidBody;
    
    private void Update()
    {
        transform.Translate(Velocity * Time.deltaTime, Space.Self);
    }
}
