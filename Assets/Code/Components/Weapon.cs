﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour 
{
	[SerializeField]
	private Transform _projectileSpawnPoint;
	
	[SerializeField]
	private GameObject _projectilePrefab;
	
	public void Fire()
	{
		var projectile = Instantiate(_projectilePrefab, _projectileSpawnPoint.position, Quaternion.identity)  as GameObject;
		projectile.transform.SetParent(transform.parent);
	}
}
