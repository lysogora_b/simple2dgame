﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _obstaclesPrefabs;
    [SerializeField]
    private float _timeBetweenSpawn;

    private bool _isRunning;

    public void LaunchSpawner()
    {
        if (_isRunning) return;
        _isRunning = true;
        StartCoroutine(SpawnerCoroutine());
        
    }

    public void StopSpawner()
    {
        _isRunning = false;
        StopAllCoroutines();
    }

    private IEnumerator SpawnerCoroutine()
    {
        // randomize coroutine start to avoid simultaneous spawning of multiple obstacle spawners
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.0f, _timeBetweenSpawn));
        while (_isRunning)
        {
            SpawnRandomObstacle();
            yield return new WaitForSeconds(_timeBetweenSpawn);
        }
    }

    private void SpawnRandomObstacle()
    {
        int r = UnityEngine.Random.Range(0, _obstaclesPrefabs.Length);
        SpawnObstacle(_obstaclesPrefabs[r], this.transform.position);
    }

    private void SpawnObstacle(GameObject prefab, Vector3 position)
    {
        var obstacle = Instantiate(prefab, position, transform.rotation) as GameObject;
        obstacle.transform.SetParent(this.transform);
    }
}
