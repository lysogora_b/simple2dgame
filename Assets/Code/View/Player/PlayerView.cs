﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour, IPlayerView, IDamagable
{
	public IPlayer Player { get; set; }

	public Transform TransformView { get { return this.transform; } }
	
	[SerializeField]
	private Weapon _weapon;

	[SerializeField]
	private float _speed;
	
	public void Move(Vector2 velocity)
	{
        this.transform.Translate(velocity * Time.deltaTime * _speed);
	}

	public void Fire()
	{
		_weapon.Fire();
	}

	public void DestroyPlayer()
	{
		Destroy(this.gameObject);
	}

	public void ReceiveDamage(int damage)
	{
		Player.HitPoints -= damage;
	}
}

public interface IPlayerView
{
	IPlayer Player { get; set; }
	Transform TransformView { get; }
	void Move(Vector2 velocity);
	void Fire();
	void DestroyPlayer();
}