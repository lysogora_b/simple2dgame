﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelView : MonoBehaviour, ILevelView
{
    [SerializeField]
    private ObstaclesSpawner[] _obstacleSpawners;
    
    [SerializeField]
    private Transform _platerSpawnPoint;
    public Transform PlayerSpawnPoint
    {
        get { return _platerSpawnPoint; }
    }

    public void InitializeLevel()
    {
        foreach (var obstacleSpawner in _obstacleSpawners)
        {
            
        }
    }
    
    public void StartLevel()
    {
        foreach (var obstacleSpawner in _obstacleSpawners)
        {
            obstacleSpawner.LaunchSpawner();
        }
    }
}

public interface ILevelView
{
    Transform PlayerSpawnPoint { get; }

    void InitializeLevel();

    void StartLevel();
}
