﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
	[SerializeField]
	private MeshRenderer _meshRenderer;

	[SerializeField]
	private float _scrollSpeed;
	
	// Update is called once per frame
	void LateUpdate ()
	{
		var newOffset = _meshRenderer.material.GetTextureOffset("_MainTex") + new Vector2(_scrollSpeed * Time.deltaTime, 0.0f);
		_meshRenderer.material.SetTextureOffset("_MainTex", newOffset );
	}
}
