﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides necessary data for the logic layer
/// </summary>
public interface IDataProvider
{
    /// <summary>
    /// FIres when level is ready for playing and provides controller of the level
    /// </summary>
    event Action<LevelController> OnLevelReady;
    /// <summary>
    /// Fires when level scene has been removed
    /// </summary>
    event Action OnLevelCleared;
    
    IPlayer GetPlayer();

    void GetLevel(string levelName);
    
    void ClearLevel(string levelName);
}
